# Podcast - Activity

## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

## Add your files

-  Switch to `developer` branch
-  Add the activity files under the `activity` folder.
-  the activity file type for Podcast should be `.mp3` file extension only.
-  the image file for the `podcast album-artwork` in `.jpg` format only . 
-  image should be a `Square image` 
-  final two objects will be in the activity folder as `activity/file.mp3` & `activity/album-art.jpg` 
-  the final deployment will look like this [https://jigyasa-csir.in/comb-act-test/n99-t1-a6/](https://jigyasa-csir.in/comb-act-test/n99-t1-a6/)
-  Large Audio file size may take too much bandwidth to load on the students laptop/mobile. Therefore, kindly keep the file size `below 100 Mb` The lesser the file size, the faster it will load on user end. 
-  To reduce the file size, you can reduce the bitrate of the audio. 

## Hosting / Deploying your activity.
-  After adding acitivity , raise a Merge Request (MR) from `developer`branch to the `main` branch.
-  `Our team will verify the MR and merge to main. `
-  Once the build pipeline passes (~10-15 mins), it will be `auto-deployed` under the url `https://jigyasa-csir.in/nodal-acronym/activity-id/ `
-  for eg: if the `nodal acronym : cgcri` and the `activity-id: n12-t1-a2`then the deployment URL will be  `https://jigyasa-csir.in/cgcri/n99-t9-a1/`

